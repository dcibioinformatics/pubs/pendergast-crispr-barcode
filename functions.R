# reused code section as a function.

toRatio <- function(count){
	for(i in 1:ncol(count))
	{
		if(is.numeric(count[,i]))
			count[,i] = count[,i]/sum(count[,i])	
	}
	count
}

geneScore.rank <- function(caseDat){
	caseDat.geneScore <- caseDat
	caseDat.geneScore$gene <- sapply(caseDat.geneScore$Construct, 
		FUN = function(x) strsplit(x, split="_")[[1]][1]) 
	caseDat.geneScore <- caseDat.geneScore[order(caseDat.geneScore$gene,-caseDat.geneScore$log2foldchange),]
	
	casedat.gene <- NULL
	for(seq in unique(caseDat.geneScore$gene)){
		tmp <- caseDat.geneScore[which(caseDat.geneScore$gene == seq),]
		tmp <- tmp[!is.na(tmp$log2foldchange),]
		barcode.No.in.tmp.gene <- nrow(tmp)
		if(mean(tmp$log2foldchange, na.rm = TRUE) <= 1 & barcode.No.in.tmp.gene > 1)
			casedat.gene <- rbind(casedat.gene, tmp[barcode.No.in.tmp.gene-1,])
		if(mean(tmp$log2foldchange, na.rm = TRUE) <= 1 & barcode.No.in.tmp.gene == 1)
			casedat.gene <- rbind(casedat.gene, tmp[barcode.No.in.tmp.gene,])
		if(mean(tmp$log2foldchange, na.rm = TRUE) > 1 & barcode.No.in.tmp.gene > 1)
			casedat.gene <- rbind(casedat.gene, tmp[2,])
		if(mean(tmp$log2foldchange, na.rm = TRUE) > 1 & barcode.No.in.tmp.gene == 1)
			casedat.gene <- rbind(casedat.gene, tmp[1,])
		if(barcode.No.in.tmp.gene == 0)
			next
	}
		
	casedat.gene.all <- casedat.gene[order(-casedat.gene$log2foldchange),
					c(6,5,1,2,3,4)]
	casedat.gene <- casedat.gene[order(-casedat.gene$log2foldchange),
					c("gene", "log2foldchange")]
	colnames(casedat.gene) <- c("gene", "geneScore")
	rownames(casedat.gene) <- NULL
	res = list(casedat.gene=casedat.gene,casedat.gene.all=casedat.gene.all)
	res 
}

geneScore <- function(caseDat){
	caseDat.geneScore <- caseDat
	caseDat.geneScore$gene <- sapply(caseDat.geneScore$Construct, 
		FUN = function(x) strsplit(x, split="_")[[1]][1]) 
	caseDat.geneScore <- caseDat.geneScore[order(caseDat.geneScore$gene,-caseDat.geneScore$log2foldchange),]
	
	casedat.gene <- NULL
	for(seq in unique(caseDat.geneScore$gene)){
		tmp <- caseDat.geneScore[which(caseDat.geneScore$gene == seq),]
		tmp <- tmp[!is.na(tmp$log2foldchange),]
		barcode.No.in.tmp.gene <- nrow(tmp)
		tmp <- caseDat.geneScore[which(caseDat.geneScore$gene == seq),]
		tmp <- tmp[!is.na(tmp$log2foldchange),]
		barcode.No.in.tmp.gene <- nrow(tmp)
		if(mean(tmp$log2foldchange, na.rm = TRUE) <= 1 & barcode.No.in.tmp.gene > 1)
		   index <- barcode.No.in.tmp.gene-1
		if(mean(tmp$log2foldchange, na.rm = TRUE) <= 1 & barcode.No.in.tmp.gene == 1)
		  index <- barcode.No.in.tmp.gene
		if(mean(tmp$log2foldchange, na.rm = TRUE) > 1 & barcode.No.in.tmp.gene > 1)
		  index <-2
		if(mean(tmp$log2foldchange, na.rm = TRUE) > 1 & barcode.No.in.tmp.gene == 1)
		  index <-1
		if(barcode.No.in.tmp.gene != 0){
		  score <- mean(tmp$log2foldchange, na.rm = TRUE)
		  tmp <- tmp[index, ]
		  tmp$log2foldchange <- score
		  casedat.gene <- rbind(casedat.gene, tmp)
		}
		if(barcode.No.in.tmp.gene == 0)
		  next
	}
	casedat.gene.all <- casedat.gene[order(-casedat.gene$log2foldchange),
	                                 c(6,5,1,2,3,4)]
	casedat.gene <- casedat.gene[order(-casedat.gene$log2foldchange),
	                             c("gene", "log2foldchange")]
	colnames(casedat.gene) <- c("gene", "geneScore")
	rownames(casedat.gene) <- NULL
	res = list(casedat.gene=casedat.gene,casedat.gene.all=casedat.gene.all)
	res 
}

foldChange <- function(finalCount, CTRL, TREAT, csv.dir)
{
	countInRatio <- toRatio(finalCount)
	caseDat <- countInRatio[,c("sg_sequence", "Construct", CTRL, TREAT)]
		
	log2foldchange <- log2(caseDat[,TREAT]/caseDat[,CTRL])	
 	log2foldchange[which(caseDat[,CTRL] == 0)] <- NA
	#log2foldchange[which(caseDat[,T0] == 0 & caseDat[,Tf] > 0)] <- Inf
	log2foldchange[which(caseDat[,TREAT] == 0 & caseDat[,CTRL] > 0)] <- -Inf

	caseDat$log2foldchange <- log2foldchange
    caseDat <- caseDat[order(-caseDat$log2foldchange), ]

    # write barcode fold change to csv file 
	write.csv( caseDat, file=
               file.path(csv.dir, 
			   paste0(CTRL,"_",TREAT, "-barcode-foldChange.csv")),
               quote=F,
               row.names=FALSE
             )

	# write completely fallout barcode to csv file
	fallout.barcode <- which(caseDat[,TREAT] == 0 & caseDat[,CTRL] > 0)
	caseDat.fallout <- caseDat[fallout.barcode, ]
	if(length(fallout.barcode) > 0){
		write.csv( caseDat.fallout, file=
               file.path(csv.dir, 
						paste0(CTRL, "_",TREAT, "-barcode-fallout.csv")
						),
               quote=F,
               row.names=FALSE
             )	
	}

	# compute gene score
	gene.score <- geneScore(caseDat)
	caseDat.gene <- gene.score$casedat.gene 

    # write gene fold change to csv file 
	write.csv( caseDat.gene, file=
               file.path(csv.dir, 
			   paste0(CTRL,"_",TREAT, "-gene-Score.csv")),
               quote=F,
               row.names=FALSE
             )

	caseDat.gene.all <- gene.score$casedat.gene.all
	fallout.gene <- which(caseDat.gene.all[,TREAT] == 0 & caseDat.gene.all[,CTRL] > 0)
	caseDat.gene.fallout <- caseDat.gene.all[fallout.gene, ]
	if(length(fallout.gene) > 0){
		write.csv( caseDat.gene.fallout, file=
               file.path(csv.dir, 
						paste0(CTRL, "_",TREAT, 
							   "-gene-fallout.csv"
							  )
						),
               quote=F,
               row.names=FALSE
             )	
	}

	# QC quality
	T0_eq_0_barcode.number <- sum(caseDat[,CTRL] == 0) 
	res <- list()
	res$barcode_foldchange <- caseDat
	res$geneScore_foldchange <- caseDat.gene
	res$geneScore_foldchange_all <- caseDat.gene.all
	res$CTRL_eq_0_No <- T0_eq_0_barcode.number
	res$CTRL = CTRL
	res$TREAT = TREAT
	res
}




snakePlot <- function(res, fig.dir){	
	# barcode snake
	barcode.caseDat <- res$barcode_foldchange
	barcode.caseDat$sgRNA <- nrow(barcode.caseDat):1
	main = paste0("Log2 fold change of barcodes for ",res$TREAT, " ( compared to ",res$CTRL, ")") 
	snakebarcode <- ggplot(barcode.caseDat , aes(x=sgRNA, y=log2foldchange)) + 
  						geom_point(aes()) + 
 						labs(
      						y="Log2 fold change", 
       						x="sgRNA", xaxt='n',
       						title=main)+
						theme(axis.text.x=element_blank())+ theme_bw()+
 						theme(plot.title = element_text(hjust = 0.5)) 
						

	pdf(file.path(fig.dir, paste0(res$TREAT, " VS ", res$CTRL, "-barcode-snake.pdf")),
		width=6,heigh=5)
	plot(snakebarcode)
	dev.off()

	# gene snake
	gene.caseDat <- res$geneScore_foldchange
    gene.caseDat$ID <- nrow(gene.caseDat):1
	main = paste0("Gene score of ", res$TREAT, " ( compared to ",res$CTRL, ")") 
	snakegene <- ggplot(gene.caseDat , aes(x=ID, y=geneScore)) + 
  						geom_point(aes()) + 
 						labs(
      						y="Log2 fold change", 
       						x="Gene", xaxt='n',
       						title=main)+
						theme(axis.text.x=element_blank())+ theme_bw()+
 						theme(plot.title = element_text(hjust = 0.5)) 
						

	pdf(file.path(fig.dir, paste0(res$TREAT, "-based-", res$CTRL, "-gene-snake.pdf")),
		width=6,heigh=5)
	print(snakegene)
	dev.off()

	res <- list(snakebarcode=snakebarcode, snakegene=snakegene)
	res
}

comp.plot.lidsky <- function(case1, case2, EssenGeneList, fig.dir){
	#barcode qqplot
	case1.dat <- case1$barcode_foldchange[,c(1,5)]
	case2.dat <- case2$barcode_foldchange[,c(1,5)]
	#colnames(case2.dat) <- c("sg_sequence","log2foldchange_2")
	mergedDat <- merge(case1.dat, case2.dat, by="sg_sequence")
	main = paste0("Log2 fold change compare for barcode in ", case1$cellline, " Tf 1 and 2 ") 
	compbarcode <- ggplot(mergedDat, aes(x=log2foldchange.x, y=log2foldchange.y)) + 
  						geom_point(aes()) + labs(y="Tf-2",x="Tf-1",title=main)+
						geom_abline(intercept =0 , slope = 1, color="blue", lty=2)+ 
 						theme_bw()+
						theme(plot.title = element_text(hjust = 0.5)) 
	pdf(file.path(fig.dir, paste0(case1$cellline, "-Tf-1-2-barcode-compare.pdf")),
		width=6,heigh=5)
	print(compbarcode)
	dev.off()

	case1.dat <- case1$geneScore_foldchange
	case2.dat <- case2$geneScore_foldchange
	#colnames(case2.dat) <- c("sg_sequence","log2foldchange_2")
	mergedDat <- merge(case1.dat, case2.dat, by="gene", all=TRUE)
	mergedDat$Gene_type <- "None Essential Gene"
	mergedDat$Gene_type[mergedDat$gene %in% EssenGeneList[,1]] <- "Essential Gene"
	main = paste0("Gene score compare for ", case1$cellline, " Tf 1 and 2 ") 
	compgene <- ggplot(mergedDat, aes(x=geneScore.x, y=geneScore.y)) + 
						scale_color_manual(values=c("red", "blue")) +
  						geom_point(aes(colour = Gene_type), size = 0.3) + 
						labs(y="Tf-2",x="Tf-1",title=main)+
						geom_abline(intercept =0 , slope = 1, color="blue", lty=2)+ 
						geom_hline(yintercept= 0, linetype="dashed", color = "yellow", size=0.5) + 
						geom_vline(xintercept= 0,linetype="dashed", color = "yellow", size=0.5) +
						theme_bw() +
						theme(plot.title = element_text(hjust = 0.5)) 
	pdf(file.path(fig.dir, paste0(case1$cellline, "-Tf-1-2-geneScore-compare.pdf")),
		width=6,heigh=5)
	print(compgene)
	dev.off()
	res <- list(compbarcode = compbarcode, compgene=compgene)
	res
}


EssenGeneFoldChange <- function(res, EssenGeneList, csv.dir){
	tmp <- res$geneScore_foldchange
	rownames(tmp) <- tmp[,1]
	tmp <-tmp[EssenGeneList[,1],]
	rownames(tmp) <- NULL
	write.csv( tmp, file=
               file.path(csv.dir, 
			   paste0(res$cellline, "-EssentialGene-Tf","-",
					  res$replicates, "-gene-Score.csv")),
               quote=F,
               row.names=FALSE
             )
}




LowReadFilter <- function(count, cutOffReadNum=100){
	#cutOffReadNum=5
	#count <- finalCount	

	#removedReadPercentage <- NULL
	#removedBarPercentage <- NULL
	#zero_CCLP_i <- NULL
	#zero_SSP_i <- NULL
	#j = 1

	for(i in c(3,8)){
		cutOff <- count[,i] < cutOffReadNum
		#removedReadPercentage[j] <- sum(count[cutOff,i])/sum(count[,i])
		#removedBarPercentage[j] <- sum(cutOff)/nrow(count)
		#zero_CCLP_i[j] <- sum(count[,i] == 0)
		#zero_SSP_i[j] <- sum(count[,i] == 0)
		count[cutOff, i] <- 0
		#j = j + 1
	}
	
	#removedReadPercentage
	#removedBarPercentage
	#zero_CCLP_i
	#zero_SSP_i 
	#count[count[,3]==0, ]
	count 
}




OverLapFallOut <- function(case1, case2, csv.dir){
	#barcode fall out overlop 
	T0 <- paste0(case1$cellline, "-","T0")
	Tf <- paste0(case1$cellline, "-","Tf","-", case1$replicates)

	case1.dat <- case1$barcode_foldchange
	case2.dat <- case2$barcode_foldchange
	case1.dat.gene <- case1$geneScore_foldchange_all
	case2.dat.gene <- case2$geneScore_foldchange_all
	
	fallout.barcode <- which(case1.dat[,Tf] == 0 & case1.dat[,T0] > 0)
	fallout.gene <- which(case1.dat.gene[,Tf] == 0 & case1.dat.gene[,T0] > 0)
	case1.dat.fallout <- case1.dat[fallout.barcode, ]
	case1.dat.gene.fallout <- case1.dat.gene[fallout.gene, ]

	Tf <- paste0(case2$cellline, "-","Tf","-", case2$replicates)

	fallout.barcode <- which(case2.dat[,Tf] == 0 & case2.dat[,T0] > 0)
	fallout.gene <- which(case2.dat.gene[,Tf] == 0 & case2.dat.gene[,T0] > 0)
	case2.dat.fallout <- case2.dat[fallout.barcode, ]
	case2.dat.gene.fallout <- case2.dat.gene[fallout.gene, ]

	case1_2_barcode.overlap.fallout <- merge(case1.dat.fallout, case2.dat.fallout[,c(1,4)], by="sg_sequence")
	case1_2_gene.overlap.fallout <- merge(case2.dat.gene.fallout, case2.dat.gene.fallout[,c(1,6)], by="gene")

	write.csv( case1_2_barcode.overlap.fallout[,1], file=
               file.path(csv.dir, 
			   paste0(case1$cellline, "-barcode-fallout-overlap.csv")),
               quote=F,
               row.names=FALSE
             )
		
	write.csv( case1_2_gene.overlap.fallout[,1], file=
               file.path(csv.dir, 
			   paste0(case1$cellline, "-gene-fallout-overlap.csv")),
               quote=F,
               row.names=FALSE
             )
	#caseDat.gene.fallout <- caseDat.gene.all[fallout.gene, ]
	res <- list(case1.barcode.fallout = case1.dat.fallout, 
				case2.barcode.fallout = case2.dat.fallout,
				case1.gene.fallout = case1.dat.gene.fallout,
				case2.gene.fallout = case2.dat.gene.fallout,
				case1_2_barcode.overlap.fallout = case1_2_barcode.overlap.fallout,
				case1_2_gene.overlap.fallout = case1_2_gene.overlap.fallout)
	res
}


commonGenes <- function(case1, case2, case3, case4, EssenGeneList, per=0.1, csv.dir)
{
	case1.gene <- case1$geneScore_foldchange
	case1.gene <- case1.gene[which(case1.gene$geneScore < 0),]
	case1.gene <- case1.gene[case1.gene$geneScore <= quantile(case1$geneScore_foldchange$geneScore, per),]

	case2.gene <- case2$geneScore_foldchange	
	case2.gene <- case2.gene[which(case2.gene$geneScore < 0),]
	case2.gene <- case2.gene[case2.gene$geneScore <= quantile(case2$geneScore_foldchange$geneScore, per),]

	case3.gene <- case3$geneScore_foldchange
	case3.gene <- case3.gene[which(case3.gene$geneScore < 0),]
	case3.gene <- case3.gene[case3.gene$geneScore <= quantile(case3$geneScore_foldchange$geneScore, per),]
	
#	case4.gene <- case4$geneScore_foldchange
#	case4.gene <- case4.gene[which(case4.gene$geneScore < 0),]
#	case4.gene <- case4.gene[case4.gene$geneScore <=quantile(case4$geneScore_foldchange$geneScore, per),]
	
	res <- merge(case1.gene, case2.gene, by="gene")
	res <- merge(res, case3.gene, by='gene')
	#res <- merge(res, case4.gene, by='gene')
	colnames(res) <- c("gene", "CCLP-TF-1", "CCLP-TF-2", "SSP-TF-1")#, "SSP-TF-2")
	res <- res[!res$gene %in% EssenGeneList[,1] ,]
	write.csv( res, file=
               file.path(csv.dir, 
			   paste0("Most_common_depleted_genes_among_all_samples_CCLPtf12SSPtf1.csv")),
               quote=F,
               row.names=FALSE
             )
	res
}


snakeGeneLabel <- function(res, EssenGeneList, fig.dir){	
	# gene snake
	gene.caseDat <- res$geneScore_foldchange
	rownames(gene.caseDat) <- gene.caseDat$gene
    gene.caseDat$ID <- nrow(gene.caseDat):1
	gene.caseDat$GeneType <- "None Essential Genes"
	gene.caseDat$size <- 0.3
	gene.caseDat[EssenGeneList[,1],"GeneType"] <- "Essential Genes"
	gene.caseDat[EssenGeneList[,1],"size"] <- 0.7

	#highlightedGenes <- c("PPP2CA", "KRAS", "CDK13", "MCL1", "ADSS", "SMAD2", "HIPK4", "CEBPA", "DHFR", "LIG1")
	highlightedGenes <- c("CACNA1G", "CACNA1A", "WNK4", "CENPM", "CENPL", "NHEJ1", "RAD51B", "LIG1", "PARP4", "PER1", "MYLK3")
	
	gene.caseDat[highlightedGenes,"GeneType"] <- "Highlighted Genes"
	gene.caseDat[highlightedGenes,"size"] <- 1.2
	main = paste0("Gene score ", res$cellline, " Tf ", res$replicates) 
	snakegene <- ggplot(gene.caseDat, aes(x=ID, y=geneScore),yrange) + 
  						geom_point(aes(colour=GeneType)) + scale_color_manual(values=c("red", "blue", "gray")) +  xlim(-500, 2550)+ theme_bw()+
						geom_point(data=gene.caseDat[EssenGeneList[,1],], aes(x=ID,y=geneScore), colour="red", size=1) + theme_bw()+
						geom_point(data=gene.caseDat[highlightedGenes,], aes(x=ID,y=geneScore), colour="blue", size=1) +theme_bw()+
						geom_label_repel(data=gene.caseDat[highlightedGenes,], aes(x=ID,y=geneScore, label=gene),
                                          segment.color = 'grey50', size=3, nudge_x = -0.35,  hjust = 1, segment.size = 0.2) +
 						labs(
      						y="Log2 fold change", 
       						x="Gene", xaxt='n',
       						title=main)+
						theme(axis.text.x=element_blank())+ 
						theme_bw()+
 						theme(plot.title = element_text(hjust = 0.5)) 
						

	pdf(file.path(fig.dir, paste0(res$cellline, "-Tf-", res$replicates, "-gene-snake-labeled.pdf")),
		width=6,heigh=5)
	print(snakegene)
	dev.off()

	res <- list(snakegene=snakegene)
	res
}






snakePlotCTRL <- function(res, fig.dir, CTRL){	
	# barcode snake
	barcode.caseDat <- res$barcode_foldchange
	barcode.caseDat$sgRNA <- nrow(barcode.caseDat):1
	barcode.caseDat$gene <-  sapply(finalCount$Construct, FUN=function(x) strsplit(x, split="_")[[1]][1])
	barcode.caseDat$GeneType <- "None control barcode"
	barcode.caseDat$GeneType[barcode.caseDat$gene %in% CTRL] <- "Control barcode"

	main = paste0("Log2 fold change of barcodes for ", res$cellline, " Tf ", res$replicates) 
	snakebarcode <- ggplot(barcode.caseDat , aes(x=sgRNA, y=log2foldchange), yrange) + 
						geom_point(aes(colour=GeneType)) + scale_color_manual(values=c("red", "blue"))+ theme_bw()+
						geom_point(data=barcode.caseDat[barcode.caseDat$GeneType == "Control barcode",], 
									aes(x=sgRNA,y=log2foldchange), colour="red", size=1) +theme_bw()+ 						
						labs(
      						y="Log2 fold change", 
       						x="sgRNA", xaxt='n',
       						title=main)+
						theme(axis.text.x=element_blank())+ theme_bw()+
 						theme(plot.title = element_text(hjust = 0.5)) 
						

	pdf(file.path(fig.dir, paste0(res$cellline, "-Tf-", res$replicates, "-barcode-snake-highlight-ctrl.pdf")),
		width=6,heigh=5)
	plot(snakebarcode)
	dev.off()

	# gene snake
	gene.caseDat <- res$geneScore_foldchange
    gene.caseDat$ID <- nrow(gene.caseDat):1
	gene.caseDat$GeneType <- "None control gene"
	gene.caseDat$GeneType[gene.caseDat$gene %in% CTRL] <- "Control gene"
	main = paste0("Gene score ", res$cellline, " Tf ", res$replicates) 
	snakegene <- ggplot(gene.caseDat , aes(x=ID, y=geneScore), yrange) + 
  						geom_point(aes(colour=GeneType)) + scale_color_manual(values=c("red", "blue"))+ theme_bw()+
						geom_point(data=gene.caseDat[gene.caseDat$GeneType == "Control gene",], 
									aes(x=ID,y=geneScore), colour="red", size=1) +theme_bw()+ 		
 						labs(
      						y="Log2 fold change", 
       						x="Gene", xaxt='n',
       						title=main)+
						theme(axis.text.x=element_blank())+ theme_bw()+
 						theme(plot.title = element_text(hjust = 0.5)) 
						

	pdf(file.path(fig.dir, paste0(res$cellline, "-Tf-", res$replicates, "-gene-snake-highlight-ctrl.pdf")),
		width=6,heigh=5)
	print(snakegene)
	dev.off()

	res <- list(snakebarcode=snakebarcode, snakegene=snakegene)
	res
}


comp.plot.CTRL <- function(case1, case2, EssenGeneList, fig.dir, CTRL){
	#barcode qqplot
	case1.dat <- case1$barcode_foldchange[,c(1,5)]
	case2.dat <- case2$barcode_foldchange[,c(1,5)]
	gene <- case1$barcode_foldchange[,c(1,2)]
	#colnames(case2.dat) <- c("sg_sequence","log2foldchange_2")
	mergedDat <- merge(case1.dat, case2.dat, by="sg_sequence")
	mergedDat <- merge(mergedDat, gene, by="sg_sequence")
	mergedDat$gene <-  sapply(mergedDat$Construct, FUN=function(x) strsplit(x, split="_")[[1]][1])
	mergedDat$GeneType <- "None control barcode"
	mergedDat$GeneType[mergedDat$gene %in% CTRL] <- "Control barcode"
	main = paste0("Log2 fold change compare for barcode in ", case1$cellline, " Tf 1 and 2 ") 
	compbarcode <- ggplot(mergedDat, aes(x=log2foldchange.x, y=log2foldchange.y), yrange) + 
						geom_point(aes(colour=GeneType)) + scale_color_manual(values=c("red", "blue"))+ theme_bw()+
						geom_point(data=mergedDat[mergedDat$GeneType == "Control barcode",], 
									aes(x=log2foldchange.x,y=log2foldchange.y), colour="red", size=1) +theme_bw()+ 
  						labs(y="Tf-2",x="Tf-1",title=main)+
						geom_abline(intercept =0 , slope = 1, color="black", lty=2)+ 
 						theme_bw()+
						theme(plot.title = element_text(hjust = 0.5)) 
	pdf(file.path(fig.dir, paste0(case1$cellline, "-Tf-1-2-barcode-compare-highlight-ctrl.pdf")),
		width=6,heigh=5)
	print(compbarcode)
	dev.off()

	case1.dat <- case1$geneScore_foldchange
	case2.dat <- case2$geneScore_foldchange
	#colnames(case2.dat) <- c("sg_sequence","log2foldchange_2")
	mergedDat <- merge(case1.dat, case2.dat, by="gene", all=TRUE)
	
	mergedDat$Gene_type <- "None Essential Gene"
	mergedDat$Gene_type[mergedDat$gene %in% EssenGeneList[,1]] <- "Essential Gene"
	mergedDat$Gene_type[mergedDat$gene %in% CTRL] <- "Control Gene"
	main = paste0("Gene score compare for ", case1$cellline, " Tf 1 and 2 ") 
	mergedDat <- mergedDat[order(mergedDat$Gene_type, decreasing = TRUE),]
	compgene <- ggplot(mergedDat, aes(x=geneScore.x, y=geneScore.y),yrange) + 
						geom_point(aes(colour = Gene_type), size = 0.3) + scale_color_manual(values=c("red", "blue","gray"))+ theme_bw() +
						geom_point(data=mergedDat[mergedDat$Gene_Type == "None Essential Gene",], 
									aes(x=geneScore.x,y=geneScore.y), colour="gray", size=0.3) +theme_bw()+ 
						geom_point(data=mergedDat[mergedDat$Gene_Type == "Essential Gene",], 
									aes(x=geneScore.x,y=geneScore.y), colour="blue", size=1) +theme_bw()+ 
						geom_point(data=mergedDat[mergedDat$Gene_Type == "Control barcode",], 
									aes(x=geneScore.x,y=geneScore.y), colour="red", size=1) +theme_bw()+ 
						labs(y="Tf-2",x="Tf-1",title=main)+
						geom_abline(intercept =0 , slope = 1, color="blue", lty=2)+ 
						geom_hline(yintercept= 0, linetype="dashed", color = "yellow", size=0.5) + 
						geom_vline(xintercept= 0,linetype="dashed", color = "yellow", size=0.5) +
						theme_bw() +
						theme(plot.title = element_text(hjust = 0.5)) 
	pdf(file.path(fig.dir, paste0(case1$cellline, "-Tf-1-2-geneScore-compare-highlight-control.pdf")),
		width=6,heigh=5)
	print(compgene)
	dev.off()
	res <- list(compbarcode = compbarcode, compgene=compgene)
	res
}



myNorm <- function(finalCount, totalNoSamples){
	caseID <- sapply(colnames(finalCount)[3:(2+totalNoSamples)], FUN=function(x) strsplit(x, split="\\-")[[1]][1])
	time   <- sapply(colnames(finalCount)[3:(2+totalNoSamples)], FUN=function(x) strsplit(x, split="\\-")[[1]][2])
	repli  <- sapply(colnames(finalCount)[3:(2+totalNoSamples)], FUN=function(x) if(length (strsplit(x, split="\\-")[[1]]) >=3 ) {strsplit(x, split="\\-")[[1]][3]} 							else{ 1 })	
	metaInfo <- data.frame( group=caseID, time=time, replicate=repli)
	dds<-DESeqDataSetFromMatrix(countData=finalCount[,3:(2+totalNoSamples)],
        colData=metaInfo, design=~group)
	featureData<-data.frame(barcode=finalCount[,1],gene=finalCount[,2])
	mcols(dds)<-DataFrame(mcols(dds), featureData)
	dds <- dds[rowSums(counts(dds))>0, ]
	dds <- estimateSizeFactors(dds)
	finalCount.normal <- counts(dds, normalized=TRUE)
	finalCount.normal <- cbind(mcols(dds),finalCount.normal)
	finalCount.normal <- as.data.frame(finalCount.normal)
	finalCount.normal
}


readGeneScore <- function(wd, finalCount){
	gene.score.files <- list.files(wd,recursive=T, pattern="-gene-Score.csv$",full.names=T)
	for( i in 1:length(gene.score.files)){        
		tmpScore <- read.csv(gene.score.files[i], header=TRUE, stringsAsFactors=FALSE)  
		if(i==1){
			geneScores <- tmpScore
		}else{
			geneScores <- merge(geneScores, tmpScore, by.x ="gene", by.y = "gene",all=TRUE)
		}
	}
	rownames(geneScores) <- geneScores$gene
	geneScores <- geneScores[,-1]
	colnames(geneScores) <- colnames(finalCount)[4:13]
	geneScores
}

filterCounts <- function(count, cutoff=100){
	t0Index <- c(3,6,9,12)
	for(i in t0Index)
	{
		if(is.numeric(count[,i]))
			count[which(count[,i] < cutoff),i] <- 0 
	}
	count

}

topEnrichGeneScores <- function(geneScores, n=15){
	topEnrichGenes <- NULL
	Genes <- rownames(geneScores)
	for(i in 1:ncol(geneScores)){
		topGeneI <- Genes[order(geneScores[,i], decreasing=TRUE)[1:n]]
		topEnrichGenes <- c(topEnrichGenes, topGeneI )
	}
	topEnrichGenes <- as.character(unique(topEnrichGenes))
	topEnrichGeneScores <- geneScores[topEnrichGenes,]
	topEnrichGeneScores
}



depletedGeneScores <- function(geneScores){
	topDepletedGenes <- NULL
	Genes <- rownames(geneScores)
	for(i in 1:ncol(geneScores)){
		topGeneD <- Genes[geneScores[,i] == -Inf]
		topDepletedGenes <- c(topDepletedGenes, topGeneD )
	}
	topDepletedGenes <- as.character(unique(topDepletedGenes))
	topDepletedGeneScores <- geneScores[topDepletedGenes,]
	topDepletedGeneScores[topDepletedGeneScores== -Inf] <- -15
	topDepletedGeneScores
}



myheatMap <- function(geneScores, fig.dir){
	geneScores[geneScores==-Inf] <- -15
	geneScores <- geneScores[complete.cases(geneScores),]
	pheatmap(geneScores, main="Gene Scores (All Genes)",fontsize_row=6,fontsize_col=9,show_rownames = FALSE,
   	        filename = paste0(fig.dir, "/heatMap-All-genes.pdf"), width = 5, height = 9,
            color= colorRampPalette(brewer.pal(n = 7, name="RdYlBu"))(100))
}
myheatMapDe <- function(geneScores, fig.dir){
	geneScores[geneScores==-Inf] <- -14
	geneScores <- geneScores[complete.cases(geneScores),]
	pheatmap(geneScores, main="Gene Scores (Depleted Genes)",fontsize_row=5,fontsize_col=9,show_rownames = TRUE,
   	        filename = paste0(fig.dir, "/heatMap-common-depleted-genes.pdf"), width = 5, height = 17,
            color= colorRampPalette(brewer.pal(n = 7, name="RdYlBu"))(100))
	geneScores_avg <- (geneScores[,c(1,3,5,7,9)] + geneScores[,c(1,3,5,7,9)+1])/2
    colnames(geneScores_avg) <- c("CCLP", "HUCCT", "RBE", "SNU1079", "SSP")
	pheatmap(geneScores_avg, main="Gene Scores (Depleted Genes)",fontsize_row=5,fontsize_col=9,show_rownames = TRUE,
            filename = paste0(fig.dir, "/heatMap-common-depleted-genes-average.pdf"), width = 5, height = 16 ,
            color= colorRampPalette(brewer.pal(n = 7, name="RdYlBu"))(100))

}


heatMap <- function(topEnrichGeneScores, fig.dir, type="Enrich"){
	topEnrichGeneScores <- topEnrichGeneScores[complete.cases(topEnrichGeneScores),]
	pheatmap(topEnrichGeneScores, main="Gene Scores(-20 means completely fall out)",
			fontsize_row=7,fontsize_col=9, cluster_cols=FALSE, cluster_rows=TRUE,
   	        filename = paste0(fig.dir, "/heatMap-fallout.pdf"), width = 5, height = 14,
            color= colorRampPalette(brewer.pal(n = 7, name="RdYlBu"))(100))
}



comp.plot.check <- function(case1, case2, EssenGeneList, fig.dir){
	#barcode qqplot
	case1.dat <- case1$barcode_foldchange[,c(1,5)]
	case2.dat <- case2$barcode_foldchange[,c(1,5)]
	#colnames(case2.dat) <- c("sg_sequence","log2foldchange_2")
	mergedDat <- merge(case1.dat, case2.dat, by="sg_sequence")
	main = paste0("Log2 fold change compare for barcode in ",case1$cellline, "-Tf-", case1$replicates,"-",case2$cellline, "-Tf-", case2$replicates)
	compbarcode <- ggplot(mergedDat, aes(x=log2foldchange.x, y=log2foldchange.y)) + 
  						geom_point(aes()) + labs(y="Tf-2",x="Tf-1",title=main)+
						geom_abline(intercept =0 , slope = 1, color="blue", lty=2)+ 
 						theme_bw()+
						theme(plot.title = element_text(hjust = 0.5)) 
	pdf(file.path(fig.dir, paste0(main, "-barcode-compare.pdf")),
		width=6,heigh=5)
	print(compbarcode)
	dev.off()

	case1.dat <- case1$geneScore_foldchange
	case2.dat <- case2$geneScore_foldchange
	#colnames(case2.dat) <- c("sg_sequence","log2foldchange_2")
	mergedDat <- merge(case1.dat, case2.dat, by="gene", all=TRUE)
	mergedDat$Gene_type <- "None Essential Gene"
	mergedDat$Gene_type[mergedDat$gene %in% EssenGeneList[,1]] <- "Essential Gene"
	main = paste0("Gene score compare for ", case1$cellline, "-Tf-", case1$replicates,"-",case2$cellline, "-Tf-", case2$replicates) 
	compgene <- ggplot(mergedDat, aes(x=geneScore.x, y=geneScore.y)) + 
						scale_color_manual(values=c("red", "blue")) +
  						geom_point(aes(colour = Gene_type), size = 0.3) + 
						labs(y="Tf-2",x="Tf-1",title=main)+
						geom_abline(intercept =0 , slope = 1, color="blue", lty=2)+ 
						geom_hline(yintercept= 0, linetype="dashed", color = "yellow", size=0.5) + 
						geom_vline(xintercept= 0,linetype="dashed", color = "yellow", size=0.5) +
						theme_bw() +
						theme(plot.title = element_text(hjust = 0.5)) 
	pdf(file.path(fig.dir, paste0(main, "-geneScore-compare.pdf")),
		width=6,heigh=5)
	print(compgene)
	dev.off()
	res <- list(compbarcode = compbarcode, compgene=compgene)
	res
}

driverMutDeltaGeneScore.old <- function(geneScores, MutSampleList, geneList=NULL){
	geneScores <- geneScores[complete.cases(geneScores), ]
	if(is.null(geneList))
		geneList <- rownames(geneScores)
	CurrentList <- NULL
	geneScoreMut_WT <- list()
	for(i in 1:length(MutSampleList)){
		CurrentList <- MutSampleList[[i]]
		CurrentList <- CurrentList[CurrentList %in% colnames(geneScores)]
		MutScore <- rowMeans(geneScores[geneList,CurrentList])
		WTScore <- rowMeans(geneScores[geneList, c(!colnames(geneScores) %in% CurrentList)])
		tmpDat <- data.frame(MutScore, WTScore)
		colnames(tmpDat) <- c("MutScore","WTScore")
		rownames(tmpDat) <- geneList
		tmpDat <- tmpDat[complete.cases(tmpDat),]
		tmpDat$MutScore[tmpDat$MutScore!= -Inf & tmpDat$WTScore != -Inf] <- tmpDat$MutScore[tmpDat$MutScore!= -Inf & tmpDat$WTScore != -Inf] - tmpDat$WTScore[tmpDat$MutScore!= -Inf & tmpDat$WTScore != -Inf]
		tmpDat$MutScore[tmpDat$MutScore == -Inf  & tmpDat$WTScore == -Inf ] <- 0
		geneScoreMut_WT[[i]] <- tmpDat
	}
	names(geneScoreMut_WT) <- names(MutSampleList)
	geneScoreMut_WT
}

heatMatpMut_WT <- function(geneScoreMut_WT, fig.dir, csv.dir, showRowN =TRUE, cutoff=rep(-1.7,5)){
	caseIDS <- names(geneScoreMut_WT)
	geneScoreMut_WT_update <- list()
	for(i in 1:length(geneScoreMut_WT)){
		geneScores <- geneScoreMut_WT[[i]]
		geneScores[geneScores==-Inf] <- -15	
		geneScores[geneScores==Inf] <- 15	
		geneScores <- geneScores[complete.cases(geneScores),]
		cases <- which(geneScores[,1] < cutoff[i])
		pheatmap(geneScores[cases,c(2:3)], main=paste0("Gene Scores With Driver Genes ", caseIDS[i]),
			fontsize_row=5,fontsize_col=9,show_rownames = showRowN,
   	        filename = paste0(fig.dir, "/heatmap_MUT_dri_", caseIDS[i], "_cutoff_", cutoff[i], "_geneScore.pdf"), 
			width = 7, height = 16, cluster_cols=FALSE, cluster_rows=TRUE,
            color= colorRampPalette(brewer.pal(n = 7, name="RdYlBu"))(100))
		write.csv(geneScores[cases,2:3], file=
               file.path(csv.dir, 
			   paste0(caseIDS[i], "_Mut_WT", "_cutoff_", cutoff[i],".csv")),
               quote=F, row.names=TRUE)
		geneScoreMut_WT_update[[i]] <- geneScores[cases,c(1:3)]
		
	}
	geneScoreMut_WT_update
}

heatMatpMut_WT_all <- function(geneScoreMut_WT, fig.dir, type="depleted",cutoff=-1.7,showRowN =TRUE){
	caseIDS <- names(geneScoreMut_WT)
	for(i in 1:length(geneScoreMut_WT)){
		tmp <- 	data.frame(genes=rownames(geneScoreMut_WT[[i]]),score=geneScoreMut_WT[[i]][,2],stringsAsFactors=FALSE)
		if(i == 1)
			geneScores=tmp
		else
			geneScores <- merge(geneScores,tmp, by="genes")
	}
		rownames(geneScores) <- geneScores[,1]
		geneScores <- geneScores[,-1]
		colnames(geneScores) <- caseIDS
		
		geneScores[geneScores==-Inf] <- -15	
		pheatmap(geneScores, main=paste0("Gene Scores ", type," With Driver Genes "),
			fontsize_row=5,fontsize_col=9,show_rownames = showRowN,
   	        filename = paste0(fig.dir, "/heatmap_MUT_dri_All_geneScore_cutoff_", cutoff, ".pdf" ), 
			width = 7, height = 16, cluster_cols=TRUE, cluster_rows=TRUE,
            color= colorRampPalette(brewer.pal(n = 7, name="RdYlBu"))(100))
}





depMutGeneList <- function(geneScoreMut_WT, csv.dir, cutoff=-0.5, samples=7,type="mut"){
	geneScorest <- NULL	

	for(i in 1:length(geneScoreMut_WT)){
		if (i == 1)
			geneScorest <- geneScoreMut_WT[[i]]
		else
			geneScorest <- cbind(geneScorest,geneScoreMut_WT[[i]])
	}

	colnames(geneScorest) <- apply(expand.grid(c("_MutScore","_WTScore"), 
								names(geneScoreMut_WT))[,c(2,1)], 1, paste0, collapse="")
	

	selectedInd <- which(rowSums(geneScorest <= cutoff) >= samples)

	write.csv( geneScorest[selectedInd,], file=
               file.path(csv.dir, 
			   paste0("Common_depleted_genes_among_all_", type, 
					  "_cutoff_", cutoff,"at_least_", samples, ".csv")),
               quote=F,
               row.names=TRUE
             )
	for(i in 1:length(geneScoreMut_WT))
			geneScoreMut_WT[[i]] <- geneScoreMut_WT[[i]][selectedInd,]
	geneScoreMut_WT
}




depletionGeneList <- function(geneScores, csv.dir, cutoff=-0.5, samples=6,type="samples"){
	overlapGenes <- data.frame(rep(1, nrow(geneScores)), stringsAsFactors=FALSE, row.names=rownames(geneScores))
	for(i in 1:ncol(geneScores)){	
		tmp <- rep(1, nrow(geneScores))
		tmp[which(geneScores[,i] < cutoff)] <- 1
		tmp[which(geneScores[,i] > cutoff)] <- 0
		overlapGenes <- cbind(overlapGenes, tmp)
	}
	overlapGenes <- overlapGenes[,-1]
	colnames(overlapGenes) <- colnames(geneScores)
	geneScores <- geneScores[rownames(overlapGenes)[which(rowSums(overlapGenes) >= samples)],]
	write.csv( geneScores, file=
               file.path(csv.dir, 
			   paste0("Common_depleted_genes_among_all_", type, "_cutoff_", cutoff,"at_least_", samples, ".csv")),
               quote=F,
               row.names=TRUE
             )
	geneScores
}


library(matrixStats)

driverMutDeltaGeneScore <- function(geneScores, MutSampleList, geneList=NULL){
	geneScores <- as.matrix(geneScores[complete.cases(geneScores), ])
	if(is.null(geneList))
		geneList <- rownames(geneScores)
	CurrentList <- NULL
	geneScoreMut_WT <- list()
	for(i in 1:length(MutSampleList)){
		CurrentList <- MutSampleList[[i]]
		CurrentList <- CurrentList[CurrentList %in% colnames(geneScores)]
		MutScore <- rowMedians(geneScores[geneList,CurrentList])
		WTScore <-  rowMedians(geneScores[geneList, c(!colnames(geneScores) %in% CurrentList)])
		tmpDat <- data.frame(MutScore, MutScore, WTScore)
		colnames(tmpDat) <- c("MutScore_avg","MutScore","WTScore")
		rownames(tmpDat) <- geneList
		tmpDat <- tmpDat[complete.cases(tmpDat),]
		tmpDat$MutScore[tmpDat$MutScore!= -Inf & tmpDat$WTScore != -Inf] <- log2(exp(tmpDat$MutScore[tmpDat$MutScore!= -Inf & tmpDat$WTScore != -Inf]) / exp(tmpDat$WTScore[tmpDat$MutScore!= -Inf & tmpDat$WTScore != -Inf]))
		tmpDat$MutScore[tmpDat$MutScore == -Inf  & tmpDat$WTScore == -Inf ] <- 0
		tmpDat$MutScore[tmpDat$MutScore != -Inf  & tmpDat$WTScore == -Inf ] <- Inf
		tmpDat$MutScore[tmpDat$MutScore == -Inf  & tmpDat$WTScore != -Inf ] <- -Inf
		tmpDat <- tmpDat[order(tmpDat$MutScore, decreasing=FALSE),]
		geneScoreMut_WT[[i]] <- tmpDat
	}
	names(geneScoreMut_WT) <- names(MutSampleList)
	geneScoreMut_WT

}


comp.plot <- function(case1, case2,  fig.dir){
	#barcode qqplot
	case1.dat <- case1$barcode_foldchange[,c(1,5)]
	case2.dat <- case2$barcode_foldchange[,c(1,5)]
	#colnames(case2.dat) <- c("sg_sequence","log2foldchange_2")
	mergedDat <- merge(case1.dat, case2.dat, by="sg_sequence")
	main = paste0("Log2 fold change compare for barcode in ", case1$TREAT, " VS ", case2$TREAT) 
	compbarcode <- ggplot(mergedDat, aes(x=log2foldchange.x, y=log2foldchange.y)) + 
  						geom_point(aes()) + labs(y=case2$TREAT,x=case1$TREAT,title=main)+
						geom_abline(intercept =0 , slope = 1, color="blue", lty=2)+ 
 						theme_bw()+
						theme(plot.title = element_text(hjust = 0.5)) 
	pdf(file.path(fig.dir, paste0(case1$TREAT, "-VS-", case2$TREAT, "-barcode-foldchange-compare.pdf")),
		width=6,heigh=5)
	print(compbarcode)
	dev.off()

	case1.dat <- case1$geneScore_foldchange
	case2.dat <- case2$geneScore_foldchange
	#colnames(case2.dat) <- c("sg_sequence","log2foldchange_2")
	mergedDat <- merge(case1.dat, case2.dat, by="gene", all=TRUE)
	#mergedDat$Gene_type <- "None Essential Gene"
	#mergedDat$Gene_type[mergedDat$gene %in% EssenGeneList[,1]] <- "Essential Gene"
	main = paste0("Gene score compare for ",case1$TREAT, " VS ", case2$TREAT) 
	compgene <- ggplot(mergedDat, aes(x=geneScore.x, y=geneScore.y)) + 
						#scale_color_manual(values=c("red", "blue")) +
  						#geom_point(aes(colour = Gene_type), size = 0.3) + 
						geom_point(aes(x=geneScore.x, y=geneScore.y), size = 0.3)+ # add on
						labs(y=case2$TREAT,x=case1$TREAT,title=main)+
						geom_abline(intercept =0 , slope = 1, color="blue", lty=2)+ 
						geom_hline(yintercept= 0, linetype="dashed", color = "yellow", size=0.5) + 
						geom_vline(xintercept= 0,linetype="dashed", color = "yellow", size=0.5) +
						theme_bw() +
						theme(plot.title = element_text(hjust = 0.5)) 
	pdf(file.path(fig.dir, paste0(case1$TREAT, "-VS-", case2$TREAT, "-barcode-geneScore-compare.pdf")),
		width=6,heigh=5)
	print(compgene)
	dev.off()
	res <- list(compbarcode = compbarcode, compgene=compgene)
	res
}







