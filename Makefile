map=CRISPR_Hattaway_Mapping.Rnw
ana=CRISPR_Hattaway_Analysis.Rnw

map:
	R -e "library(knitr); knit2pdf('$(map)')"
analysis:
	R -e "library(knitr); knit2pdf('$(ana)')"


clean:
	\rm -rf *.log *.nav *.aux *.tex *.out *.snm *.toc *.vrb *.bbl
