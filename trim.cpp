#include <vector>
#include <iostream>
#include <math.h>
#include <numeric>
#include <map>
#include<fstream>

using namespace std;

int main(int argc, char *argv[])
{	
	int length = 0;
	string ID, seq, plus,Phred;
	vector<string> library;
	ifstream myfile (argv[1]);
    ofstream outfile (argv[2]);
	
	std::size_t found5end;
    
	//string frank5 = "TCTTGTGGAAAGGACGAAACACCG";
	string frank5 =   "TCTTGTGGAAAGGACGAAACACCG";

	if (myfile.is_open())
    {
      while ( getline (myfile,ID) )
      {
        getline (myfile,seq);
		getline (myfile,plus);
        getline (myfile,Phred);
	
		found5end = seq.find(frank5);
		
		if(found5end != std::string::npos && seq.length()-44>=found5end)
		{	outfile << ID <<endl;
			outfile << seq.substr(found5end+24, 20) <<endl;
			outfile << plus <<endl;
			outfile << Phred.substr(found5end+24, 20) <<endl;		
    	}  
	}
      myfile.close();
    }

	return 0;
}
